<?php

namespace SeamlessHR\AuditLog\Enums;

use SeamlessHR\SoaUtils\Helpers\EnumManager;

class AuditActionsEnum extends EnumManager
{
    const CREATED = 'Created';
    const UPDATED = 'Updated';
    const DELETED = 'Deleted';
    const ACTIVATE = 'Activate';
    const DEACTIVATE = 'Deactivate';
    const LOGIN = 'Login';
    const LOGOUT = 'Logout';
    const BULK_UPLOAD = 'Bulk Upload';
    const BULK_UPDATE = 'Bulk Update';
    const DOWNLOAD = 'Download';
    const RESTORED = 'Restored';
}
