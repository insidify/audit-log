<?php

namespace SeamlessHR\AuditLog\Tests\Unit;

use PHPUnit\Framework\TestCase;
use SeamlessHR\AuditLog\AuditLog;

class AuditlogTest extends TestCase
{   
     /**
     * @test all values returns Yes
     */
    public function testAllValueReturnYes()
    {
        $auditLog = new AuditLog();

        $data = [
            'payroll_cycle_id' => '742',
            'is_taxable' => '1',
            'is_grossup' => '1',
            'amount' => 0,
            'basis' => 'fixed amount',
            'basis_value' => '1938383',
            'ledger_code' => 'true',
            'run_exit_computation' => true,
        ];

        $testAllValueReturnYes = $auditLog::checkIfValuesAreOneOrZero($data);

        $response = [
            'payroll_cycle_id' => '742',
            'is_taxable' => 'Yes',
            'is_grossup' => 'Yes',
            'amount' => 0,
            'basis' => 'fixed amount',
            'basis_value' => '1938383',
            'ledger_code' => 'Yes',
            'run_exit_computation' => 'Yes',
        ];

        $this->assertEquals($response, $testAllValueReturnYes);
    }

    /**
     * @test all values returns No
     */
    public function testAllValueReturnNo()
    {
        $auditLog = new AuditLog();
        
        $data = [
            'payroll_cycle_id' => '742',
            'is_taxable' => '0',
            'is_grossup' => '0',
            'amount' => 0,
            'basis' => 'fixed amount',
            'basis_value' => '1938383',
            'ledger_code' => 'false',
            'run_exit_computation' => false,
        ];

        $testAllValueReturnNo = $auditLog::checkIfValuesAreOneOrZero($data);

        $response = [
            'payroll_cycle_id' => '742',
            'is_taxable' => 'No',
            'is_grossup' => 'No',
            'amount' => 0,
            'basis' => 'fixed amount',
            'basis_value' => '1938383',
            'ledger_code' => 'No',
            'run_exit_computation' => 'No',
        ];

        $this->assertEquals($response, $testAllValueReturnNo);
    }
}