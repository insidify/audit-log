<?php

namespace SeamlessHR\AuditLog;

use Illuminate\Support\Arr;
use SeamlessHR\AuditLog\Jobs\AuditLogJob;
use SeamlessHr\SoaRequest\Facades\SoaCommon;
use SeamlessHR\AuditLog\Jobs\AuditLogMarketplaceJob;

class AuditLog
{
    const PAYROLL_FK = ['cost_centre_id', 'paygrade_id', 'paygroup_id', 'allowance_id', 'deduction_id', 'tax_relief_type'];

    const ORG_FK = ['location_id', 'job_role_id', 'currency_id', 'state_of_residence', 'value', 'department_id'];

    public static function logActon($data)
    {
        if (!isset($data['company_id'])){
            $data['company_id'] = request()->header('company-id');
        }

        if (!isset($data['is_shr_admin'])){
            $data['is_shr_admin'] = request()->is_shr_admin ?? false;
        }

        if (!isset($data['shr_admin_username'])){
            $data['shr_admin_username'] = request()->shr_admin_username ?? null;
        }

        if (!isset($data['old_values'])) {
            $data['old_values'] = [];
        }

        if (!isset($data['new_values'])) {
            $data['new_values'] = [];
        }

        if (!isset($data['file_id'])) {
            $data['file_id'] = null;
        }

        $data['auth'] = request()->header('Authorization');
        AuditLogJob::dispatch($data);
    }

    public static function marketplaceLogAction($data)
    {
        if(!isset($data['company_id'])){
            $data['company_id'] = request()->header('company-id');
        }
        $data['auth'] = request()->header('Authorization');
        AuditLogMarketplaceJob::dispatch($data);
    }


    public static function processLogInfo($model, $log_action, $request = null)
    {
        $changes = $model->getAuditableData();
        $old_changes = null;
        $new_changes = null;

        if (!empty($changes['old_values'])) {
            $old_changes = self::replaceOrganisationForeignKeys($changes['old_values']);
            $old_changes = self::replacePayrollForeignKeys($old_changes);
            $old_changes = self::excludePropertiesFromChanges($old_changes);
            $old_changes = self::checkIfValuesAreOneOrZero($old_changes);
        }

        if (!empty($changes['new_values'])) {
            $new_changes = self::replaceOrganisationForeignKeys($changes['new_values']);
            $new_changes = self::replacePayrollForeignKeys($new_changes);
            $new_changes = self::excludePropertiesFromChanges($new_changes);
            $new_changes = self::checkIfValuesAreOneOrZero($new_changes);
        }

        $data = array_merge($log_action, ['ip' => request()->ip(), 'old_values' => $old_changes, 'new_values' => $new_changes]);
        self::logActon($data);
    }

    protected static function replacePayrollForeignKeys($changes)
    {
        $payload = Arr::only($changes, self::PAYROLL_FK);
        if (empty($payload)) return $changes;

        $response = optional(SoaCommon::findCostCenterPaygrade($payload))->data;
        if (is_null($response)) return $changes;

        foreach ($payload as $key => $value) {
            switch($key) {
                case 'cost_centre_id':
                    $changes['cost_centre'] = optional($response->costcentre)->name ?? '---';
                    unset($changes[$key]);
                break;
                case 'paygrade_id':
                    $changes['paygrade'] = optional($response->paygrade)->name ?? '---';
                    unset($changes[$key]);
                break;
                case 'pay_group_id':
                    $changes['paygroup'] = optional($response->paygroup)->name ?? '---';
                    unset($changes[$key]);
                break;
                case 'allowance_id':
                    $changes['allowance'] = optional($response->allowance)->name ?? '---';
                    unset($changes[$key]);
                break;
                case 'deduction_id':
                    $changes['deduction'] = optional($response->deduction)->name ?? '---';
                    unset($changes[$key]);
                break;
                case 'tax_relief_type':
                    $changes[$key] = optional($response->tax_relief_type)->name ?? '---';
                break;
            }
        }

        return $changes;
    }

    protected static function replaceOrganisationForeignKeys($changes)
    {
        $payload = Arr::only($changes, self::ORG_FK);

        if (isset($payload['state_of_residence'])) {
            $payload['state_of_residence_id'] = $payload['state_of_residence'];
            unset($payload['state_of_residence']);
        }

        if (empty($payload)) return $changes;

        $response = optional(SoaCommon::findJobRoleLocation($payload))->data;
        if (is_null($response)) return $changes;

        foreach ($payload as $key => $value) {
            switch($key) {
                case 'location_id':
                    $changes['location'] = optional($response->location)->name ?? '---';
                    unset($changes[$key]);
                break;
                case 'job_role_id':
                    $changes['job_role'] = optional($response->job_role)->name ?? '---';
                    unset($changes[$key]);
                break;
                case 'currency_id':
                    $changes['currency'] = optional($response->currency)->name ?? '---';
                    unset($changes[$key]);
                break;
                case 'state_of_residence_id':
                    $changes['state_of_residence'] = optional($response->state_of_residence)->name ?? '---';
                break;
                case 'department_id':
                    $changes['department'] = optional($response->department)->name ?? '---';
                    unset($changes[$key]);
                break;
            }
        }

        return $changes;
    }

    protected static function excludePropertiesFromChanges($changes)
    {
        $exclude = ['id', 'employee_id', 'company_id', 'country_id'];

        foreach ($exclude as $value) {
            unset($changes[$value]);
        }

        return $changes;
    }

    public static function checkIfValuesAreOneOrZero($changes)
    {
        foreach($changes as $key => $value){
            if( $value === false || $value === 'false' || $value === '0'){ 
                $changes[$key] = "No";
            }else if($value === true || $value === 'true' || $value === '1'){
                $changes[$key] = "Yes";
            }else{
                $changes[$key] = $value;
            }
        }
        return $changes;
    }
}
