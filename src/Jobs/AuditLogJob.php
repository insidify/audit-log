<?php

namespace SeamlessHR\AuditLog\Jobs;

use App\Models\ExceptionalAllowance;
use App\Models\ExceptionalDeduction;
use App\Models\RecurringPayroll;
use App\Models\TaxRelief;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use SeamlessHr\SoaRequest\Facades\SoaCommon;
use SeamlessHr\SoaRequest\Facades\SoaRequest;

class AuditLogJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (isset($this->data['bulk_ids']) && !empty($this->data['bulk_ids'])) {
            $old_values = [];
            if ($this->data['request_model'] === 'ExceptionalAllowance') {
                $records = ExceptionalAllowance::withTrashed()
                    ->with(['allowance' => function($query) {
                        return $query->withTrashed();
                    }])
                    ->find($this->data['bulk_ids']);

                $employees = $this->getEmployeeInfo($records->pluck('employee_id')->toArray());

                foreach ($records as $record) {
                    $employee = $employees->firstWhere('id', $record->employee_id);
                    $name = ($employee) ? $employee->first_name.' '.$employee->last_name : '-';
                    $old_values[$name] = optional($record->allowance)->name ?? '-';
                }
            } elseif ($this->data['request_model'] === 'ExceptionalDeduction') {
                $records = ExceptionalDeduction::withTrashed()
                    ->with(['deduction' => function($query) {
                        return $query->withTrashed();
                    }])
                    ->find($this->data['bulk_ids']);

                $employees = $this->getEmployeeInfo($records->pluck('employee_id')->toArray());

                foreach ($records as $record) {
                    $employee = $employees->firstWhere('id', $record->employee_id);
                    $name = ($employee) ? $employee->first_name.' '.$employee->last_name : '-';
                    $old_values[$name] = optional($record->deduction)->name ?? '-';
                }
            } elseif ($this->data['request_model'] === 'TaxRelief') {
                $records = TaxRelief::withTrashed()->find($this->data['bulk_ids']);
                $employees = $this->getEmployeeInfo($records->pluck('employee_id')->toArray());

                foreach ($records as $record) {
                    $employee = $employees->firstWhere('id', $record->employee_id);
                    $name = ($employee) ? $employee->first_name.' '.$employee->last_name : '-';
                    $old_values[$name] = $record->name;
                }
            } elseif ($this->data['request_model'] === 'RecurringPayroll') {
                $records = RecurringPayroll::withTrashed()->find($this->data['bulk_ids']);
                $employees = $this->getEmployeeInfo($records->pluck('employee_id')->toArray());

                foreach ($records as $record) {
                    $employee = $employees->firstWhere('id', $record->employee_id);
                    $name = ($employee) ? $employee->first_name.' '.$employee->last_name : '-';
                    $old_values[$name] = $record->title;
                }
            }
            $this->data['old_values'] = $old_values;
        }

        $response = SoaRequest::postRequest('/api/v1/audit/log-action', $this->data, [
            'Authorization: '.$this->data['auth'],
            'company-id: '.$this->data['company_id']
        ]);
    }

    protected function getEmployeeInfo($employee_ids)
    {
        $employees = optional(SoaCommon::getMiniEmployees(['id' => $employee_ids]))->data;
        return collect($employees);
    }
}
