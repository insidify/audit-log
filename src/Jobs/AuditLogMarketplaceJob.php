<?php

namespace SeamlessHR\AuditLog\Jobs;

use Excel;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use SeamlessHR\SoaUtils\Facades\SoaUtils;
use SeamlessHR\SoaUtils\Interfaces\StatusCode;
use SeamlessHr\SoaRequest\Facades\SoaRequest;

class AuditLogMarketplaceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $response = SoaRequest::postRequest('/api/v1/audit/log-marketplace-action', $this->data, [
          "Authorization: " . $this->data['auth'],
          "company-id: " . $this->data['company_id']
      ]);
    }
}
