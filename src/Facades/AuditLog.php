<?php

namespace SeamlessHR\AuditLog\Facades;

use Illuminate\Support\Facades\Facade;

class AuditLog extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'auditlog';
    }
}
