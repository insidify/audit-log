<?php

namespace SeamlessHR\AuditLog;

trait SoaAuditableTrait
{
    protected $auditableRecord =  [
        'old_values' => [],
        'new_values' => [],
    ];

    public function transformAudit(array $data): array
    {
        $this->auditableRecord = $data;
        return $data;
    }

    public function getAuditableData()
    {
        return $this->auditableRecord;
    }
}
